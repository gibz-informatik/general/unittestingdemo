# Unit Testing
Dieses Projekt wird im Unterricht genutzt um die Erstellung von Unit Tests zu demonstrieren.

**Es wird dabei vorausgesetzt, dass die Lernenden über die Grundlegenden Techniken und Vorgehensweisen bezüglich Unit Testing bereits informiert sind**.

## Eingesetzte Tools
Für die Umsetzung und Ausführung der Unit Tests werden verschiedene Tools eingesetzt. Sämtliche nachfolgend genannten Hilfsmittel können als NuGet-Packages zum Projekt hinzugefügt werden:

- xUnit: Open Source Testing Tool für .NET Framework
- xUnit Runner für Visual Studio: Damit können die Unit Tests direkt über das GUI von Visual Studio ausgeführt und überwacht werden
- Moq: Mocking Bibliothek für .NET
- ReSharper / dotCover: Tool zum Messen der Code Coverage (Details: siehe unten)

## Links
Die nachfolgenden Links sind hilfreich für den (Wieder-)Einstieg ins Unit Testing:

- Was sind Unit Tests: https://docs.microsoft.com/en-us/dotnet/core/testing/
- Best Practices für Unit Testing: https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-best-practices
- Unit Testing C# in .NET Core mit xUnit: https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test
- Walkthrough (_Achtung: MSTest statt xUnit_): https://docs.microsoft.com/en-us/visualstudio/test/walkthrough-creating-and-running-unit-tests-for-managed-code?view=vs-2019
- Offizielle Webseite xUnit: https://xunit.net/ 
- Code Repository / Dokumentation Moq Bibliothek: https://github.com/moq/moq4
- Sehr guter Artikel für Unit Testing mit C#, WPF, MVVM, xUnit und Moq: https://entwickler.de/online/windowsdeveloper/viewmodels-tdd-mvvm-301315.html

## ReSharper
ReSharper ist ein Tool von JetBrains, welches die Produktivität von Entwicklerinnen und Entwicklern steigern soll. Es bringt viele nützliche Hilfsmittel und Zusätze für Visual Studio mit.

Für Lernende des GIBZ kann eine Ausbildungslizenz für ReSharper (und zahlreiche andere JetBrains-Produkte) _kostenlos_ erworben werden. Melden Sie sich dazu mit Ihrer _@online.gibz.ch_ Adresse online auf https://www.jetbrains.com/shop/eform/students an.