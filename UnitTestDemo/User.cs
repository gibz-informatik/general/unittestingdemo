using System.Linq;

namespace UnitTestDemo
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public string GetDomainOfEmail()
        {
            return Email.Split('@')[1];
        }

        public bool UsernameContainsSpecialChars()
        {
            char[] specialChars = {'.', '_', '-'};
            return specialChars.Any(item => Username.Contains(item));
        }
    }
}