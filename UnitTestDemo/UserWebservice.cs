using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace UnitTestDemo
{
    public class UserWebservice
    {
        private readonly string Url = "https://jsonplaceholder.typicode.com/users";

        public List<User> LoadUsers()
        {
            // Initialize an HttpWebRequest for the current URL.
            HttpWebRequest webReq = (HttpWebRequest) WebRequest.Create(Url);
            webReq.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) webReq.GetResponse();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return default;
            }

            var responseStream = response.GetResponseStream();
            if (responseStream == null)
            {
                return default;
            }

            var streamReader = new StreamReader(responseStream);
            var responseContent = streamReader.ReadToEnd().Trim();
            return JsonConvert.DeserializeObject<List<User>>(responseContent);
        }
    }
}