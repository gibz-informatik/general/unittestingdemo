using System.Collections.Generic;

namespace UnitTestDemo
{
    public class UserListPresenter
    {
        public List<User> Users { get; private set; }

        public void PrintUsers()
        {
            Load();
            if (Users.Count > 0)
            {
                System.Console.WriteLine("{0} | {1} | EMAIL", "USERNAME".PadRight(20, ' '), "NAME".PadRight(30, ' '));
                System.Console.WriteLine("=".PadRight(90, '='));
                foreach (User user in Users)
                {
                    System.Console.WriteLine("{0} | {1} | {2}", user.Username.PadRight(20, ' '),
                        user.Name.PadRight(30, ' '), user.Email);
                }
            }
            else
            {
                System.Console.WriteLine("No users available!");
            }
        }

        private void Load()
        {
            UserWebservice webservice = new UserWebservice();
            Users = webservice.LoadUsers();
        }
    }
}