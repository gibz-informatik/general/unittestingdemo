﻿using System;

namespace UnitTestDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            UserListPresenter presenter = new UserListPresenter();
            presenter.PrintUsers();
        }
    }
}